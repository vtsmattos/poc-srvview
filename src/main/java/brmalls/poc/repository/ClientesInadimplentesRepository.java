package brmalls.poc.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import brmalls.poc.model.ClientesInadimplentes;

@Repository
public interface ClientesInadimplentesRepository extends CrudRepository<ClientesInadimplentes, Long>  {

	
	List<ClientesInadimplentes> findByMesRefAndDueDateLessThanEqualAndCustomerIdIn(String mesRef, Date dueDate, List<Long> customerIds);

	List<ClientesInadimplentes> findByShoppingAndMesRefAndDueDateLessThanEqual(String shopping, String mesRef, Date dueDate);

	List<ClientesInadimplentes> findTop10By();

}
