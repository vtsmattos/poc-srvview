package brmalls.poc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "xxbrm_clientes_inadimp_v")
public class ClientesInadimplentes {

	@Id	
	@Column(name = "customer_id", nullable = false)
	private Long customerId;
	
	@Column(name = "shopping")	
	private String shopping;

	@Column(name = "mes_ref")	
	private String mesRef;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "due_date")	
	private Date dueDate;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getShopping() {
		return shopping;
	}

	public void setShopping(String shopping) {
		this.shopping = shopping;
	}

	public String getMesRef() {
		return mesRef;
	}

	public void setMesRef(String mesRef) {
		this.mesRef = mesRef;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	
}
