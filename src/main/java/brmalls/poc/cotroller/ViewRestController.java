package brmalls.poc.cotroller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import brmalls.poc.model.ClientesInadimplentes;
import brmalls.poc.proxy.ClientesInadimplentesProxy;

@RestController
@RequestMapping("/view")
public class ViewRestController {

	@Autowired
	ClientesInadimplentesProxy clientesInadimplentesProxy;
	
	@GetMapping("/top10")
	public List<ClientesInadimplentes> findTop10() throws Exception{
		List<ClientesInadimplentes> clientes = clientesInadimplentesProxy.findTop10();
		return clientes;				
	}

		
	@GetMapping("/{shopping}/{mesRef}/{dueDate}")
	public List<ClientesInadimplentes> primeiroFiltro(@PathVariable("shopping") String shopping,@PathVariable("mesRef") String mesRef,@PathVariable("dueDate") String dueDate) throws Exception{
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

		//String shopping = "NORTESHOPPING";
		//String mesRef = "04-2019";
		//Date dueDate = new Date(119,3,11);//11/4/2019
		Date dueDateFormat;
		try {
			dueDateFormat = formatter.parse(dueDate);
		} catch (ParseException e) {
			throw new Exception("Erro ao converter data");			
		}

		
		List<ClientesInadimplentes> clientes = clientesInadimplentesProxy.findByShoppingAndMesRefAndDueDateLessThanEqual(shopping, mesRef, dueDateFormat);
		return clientes;		
	}
	
	@GetMapping("/{mesRef}/{dueDate}")
	public List<ClientesInadimplentes> segudoFiltro(@PathVariable("mesRef") String mesRef,@PathVariable("dueDate") String dueDate) throws Exception{
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

		List<Long> customerIds = new ArrayList<Long>();
		Date dueDateFormat;
		try {
			dueDateFormat = formatter.parse(dueDate);
		} catch (ParseException e) {
			throw new Exception("Erro ao converter data");			
		}
		
		customerIds.add(7854049L);
		customerIds.add(2594735L);
		customerIds.add(4547739L);
		customerIds.add(8259137L);
		customerIds.add(6400125L);
		customerIds.add(10008157L);
		customerIds.add(7451L);
		customerIds.add(11818160L);
		customerIds.add(7854038L);
		customerIds.add(7848138L);
		customerIds.add(521917L);
		customerIds.add(295695L);
		
		List<ClientesInadimplentes> clientes = clientesInadimplentesProxy.findByMesRefAndDueDateLessThanEqualAndCustomerIdIn(mesRef, dueDateFormat, customerIds);
		return clientes;		
	}
	
	@GetMapping
	public String ok(){
		return "OK";
	}
	

}
