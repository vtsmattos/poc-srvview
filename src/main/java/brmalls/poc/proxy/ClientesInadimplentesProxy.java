package brmalls.poc.proxy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import brmalls.poc.model.ClientesInadimplentes;
import brmalls.poc.repository.ClientesInadimplentesRepository;

@Service
public class ClientesInadimplentesProxy {

	@Autowired
	private ClientesInadimplentesRepository clientesInadimplentesRepository;
	
	@HystrixCommand(fallbackMethod = "findByShoppingWithIgnoreCaseAndMesRefAndDueDateAfterFallback", 
			commandProperties = {
					@HystrixProperty(name="execution.isolation.strategy", value="THREAD"),
					@HystrixProperty(name="circuitBreaker.requestVolumeThreshold", value="5"),
					@HystrixProperty(name="requestCache.enabled", value="false"),
			},threadPoolProperties = {
					@HystrixProperty(name="coreSize", value="5"),
					@HystrixProperty(name="maximumSize", value="5")
			})
	public List<ClientesInadimplentes> findByShoppingAndMesRefAndDueDateLessThanEqual(String shopping,String mesRef, Date dueDate) {
		
		return clientesInadimplentesRepository.findByShoppingAndMesRefAndDueDateLessThanEqual(shopping, mesRef, dueDate);
	}
	
	List<ClientesInadimplentes> findByShoppingWithIgnoreCaseAndMesRefAndDueDateAfterFallback(String shopping,String mesRef, Date dueDate) {
		return new ArrayList<ClientesInadimplentes>();
	}
	

	@HystrixCommand(fallbackMethod = "findByMesRefAndDueDateAfterAndCustomerIdInFallback", 
			commandProperties = {
					@HystrixProperty(name="execution.isolation.strategy", value="THREAD"),
					@HystrixProperty(name="circuitBreaker.requestVolumeThreshold", value="5"),
					@HystrixProperty(name="requestCache.enabled", value="false"),
			},threadPoolProperties = {
					@HystrixProperty(name="coreSize", value="5"),
					@HystrixProperty(name="maximumSize", value="5")
			})
	public List<ClientesInadimplentes> findByMesRefAndDueDateLessThanEqualAndCustomerIdIn(String mesRef, Date dueDate,List<Long> customerIds) {
		
		return clientesInadimplentesRepository.findByMesRefAndDueDateLessThanEqualAndCustomerIdIn(mesRef, dueDate, customerIds);
	}
	
	List<ClientesInadimplentes> findByMesRefAndDueDateAfterAndCustomerIdInFallback(String mesRef, Date dueDate,List<Long> customerIds){
		return new ArrayList<ClientesInadimplentes>();
	}

	public List<ClientesInadimplentes> findTop10() {
		return clientesInadimplentesRepository.findTop10By();
	}
}
